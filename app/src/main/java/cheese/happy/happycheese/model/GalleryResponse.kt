package cheese.happy.happycheese.model


import com.google.gson.annotations.SerializedName

data class GalleryResponse(
    @SerializedName("data")
    val data: GalleryData,
    @SerializedName("errors")
    val errors: List<Any>,
    @SerializedName("msg")
    val msg: String,
    @SerializedName("status")
    val status: Boolean
)