package cheese.happy.happycheese.model


import com.google.gson.annotations.SerializedName

data class Gallery(
    @SerializedName("created_at")
    val createdAt: String,
    @SerializedName("id")
    val id: Int,
    @SerializedName("image")
    val image: Image,
    @SerializedName("languages")
    val languages: List<Language>,
    @SerializedName("updated_at")
    val updatedAt: String
)