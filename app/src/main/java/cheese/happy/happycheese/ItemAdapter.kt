package cheese.happy.happycheese

import android.animation.Animator
import android.animation.ObjectAnimator
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import cheese.happy.happycheese.model.FeaturesResponse
import cheese.happy.happycheese.network.AppApi
import com.bumptech.glide.Glide
import com.github.florent37.shapeofview.shapes.ArcView

class ItemAdapter(
    val context: Context?,
    val features: List<FeaturesResponse.Data.Feature>,
    val onItemClickListener: OnItemClickListener
) :
    RecyclerView.Adapter<ItemAdapter.ItemViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemViewHolder =
        ItemViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_layout, parent, false))

    override fun getItemCount(): Int = features.size


    override fun onBindViewHolder(holder: ItemViewHolder, position: Int) {
        holder.title.text = features[position].languages[0].title
        holder.desc.text = features[position].languages[0].description
        Glide.with(holder.itemView.context).load(AppApi.FEATURE_IMG_URL + features[position].image.name)
            .into(holder.image)
        Glide.with(holder.itemView.context).load(AppApi.FEATURE_IMG_URL + features[position].icon.name)
            .into(holder.icon)
        holder.itemLayout.setOnClickListener {
            onItemClickListener.onItemClicked(position, holder)

        }
    }

    interface OnItemClickListener {
        fun onItemClicked(position: Int, holder: ItemViewHolder)
    }

    class ItemViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val itemLayout: ArcView = itemView.findViewById(R.id.item_layout)
        val title: TextView = itemView.findViewById(R.id.title)
        val desc: TextView = itemView.findViewById(R.id.desc)
        val image: ImageView = itemView.findViewById(R.id.offer_image)
        val icon: ImageView = itemView.findViewById(R.id.icon)
    }
}