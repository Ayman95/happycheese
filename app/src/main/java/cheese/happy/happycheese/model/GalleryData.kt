package cheese.happy.happycheese.model


import com.google.gson.annotations.SerializedName

data class GalleryData(
    @SerializedName("galleries")
    val galleries: List<Gallery>
)