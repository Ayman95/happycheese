package cheese.happy.happycheese.network

import cheese.happy.happycheese.model.*
import io.reactivex.Observable
import retrofit2.http.Field
import retrofit2.http.FormUrlEncoded
import retrofit2.http.GET
import retrofit2.http.POST

interface AppApiService {

    @FormUrlEncoded
    @POST("contact-us")
    fun contactus(
        @Field("name") name: String,
        @Field("email") email: String,
        @Field("phone") phone: String,
        @Field("title") title: String,
        @Field("message") message: String
    ): Observable<ContactusResponse>

    @GET("galleries")
    fun getGallery(): Observable<GalleryResponse>

    @GET("features")
    fun getFeatures(): Observable<FeaturesResponse>

    @GET("sliders")
    fun getSlider(): Observable<SliderResponse>


    @GET("model/setting")
    fun getSettings(): Observable<SettingsResponse>
}