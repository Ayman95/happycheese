package cheese.happy.happycheese.model


import com.google.gson.annotations.SerializedName

data class Language(
    @SerializedName("created_at")
    val createdAt: String,
    @SerializedName("description")
    val description: String,
    @SerializedName("id")
    val id: Int,
    @SerializedName("lang")
    val lang: String,
    @SerializedName("title")
    val title: String
)