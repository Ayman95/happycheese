package cheese.happy.happycheese

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.recyclerview.widget.RecyclerView
import cheese.happy.happycheese.model.Gallery
import cheese.happy.happycheese.network.AppApi
import com.bumptech.glide.Glide

class GalleryImageAdapter(val onGalleryItemClickListener: OnGalleryItemClickListener, val galleries: List<Gallery>) :
    RecyclerView.Adapter<GalleryImageAdapter.GalleryItemView>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): GalleryItemView =
        GalleryItemView(LayoutInflater.from(parent.context).inflate(R.layout.gallery_item_layout, parent, false))

    override fun getItemCount(): Int = galleries.size

    override fun onBindViewHolder(holder: GalleryItemView, position: Int) {

        Glide.with(holder.itemView.context).load(AppApi.GALLERY_IMG_URL + galleries[position].image.name)
            .into(holder.galleryImage)

        holder.galleryImage.setOnClickListener {
            onGalleryItemClickListener.onGalleryItemClicked(galleries[position].image.name)
        }
    }

    interface OnGalleryItemClickListener {
        fun onGalleryItemClicked(imgName: String) {

        }
    }


    class GalleryItemView(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val galleryImage: ImageView = itemView.findViewById(R.id.gallery_image)
    }
}