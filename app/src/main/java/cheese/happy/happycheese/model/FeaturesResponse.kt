package cheese.happy.happycheese.model


import com.google.gson.annotations.SerializedName

data class FeaturesResponse(
    @SerializedName("data")
    val `data`: Data,
    @SerializedName("errors")
    val errors: List<Any>,
    @SerializedName("msg")
    val msg: String,
    @SerializedName("status")
    val status: Boolean
) {
    data class Data(
        @SerializedName("features")
        val features: List<Feature>
    ) {
        data class Feature(
            @SerializedName("created_at")
            val createdAt: String,
            @SerializedName("icon")
            val icon: Icon,
            @SerializedName("id")
            val id: Int,
            @SerializedName("image")
            val image: Image,
            @SerializedName("languages")
            val languages: List<Language>,
            @SerializedName("updated_at")
            val updatedAt: String
        ) {
            data class Image(
                @SerializedName("created_at")
                val createdAt: String,
                @SerializedName("id")
                val id: Int,
                @SerializedName("imagable_id")
                val imagableId: String,
                @SerializedName("imagable_type")
                val imagableType: String,
                @SerializedName("name")
                val name: String,
                @SerializedName("updated_at")
                val updatedAt: String
            )

            data class Icon(
                @SerializedName("created_at")
                val createdAt: String,
                @SerializedName("iconable_id")
                val iconableId: String,
                @SerializedName("iconable_type")
                val iconableType: String,
                @SerializedName("id")
                val id: Int,
                @SerializedName("name")
                val name: String,
                @SerializedName("updated_at")
                val updatedAt: String
            )

            data class Language(
                @SerializedName("created_at")
                val createdAt: String,
                @SerializedName("description")
                val description: String,
                @SerializedName("id")
                val id: Int,
                @SerializedName("lang")
                val lang: String,
                @SerializedName("title")
                val title: String
            )
        }
    }
}