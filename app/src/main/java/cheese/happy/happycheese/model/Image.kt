package cheese.happy.happycheese.model


import com.google.gson.annotations.SerializedName

data class Image(
    @SerializedName("created_at")
    val createdAt: String,
    @SerializedName("id")
    val id: Int,
    @SerializedName("imagable_id")
    val imagableId: String,
    @SerializedName("imagable_type")
    val imagableType: String,
    @SerializedName("name")
    val name: String,
    @SerializedName("updated_at")
    val updatedAt: String
)