package cheese.happy.happycheese

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.Fragment
import cheese.happy.happycheese.network.AppApi
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers

class AboutUsFragment : Fragment() {
    private val compositeDisposable = CompositeDisposable()
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.about_us_fragment, container, false)
        val aboutusText: TextView = view.findViewById(R.id.about_us_text)
        val ourGoals: TextView = view.findViewById(R.id.our_goals)
        val ourMessage: TextView = view.findViewById(R.id.our_message)
        val ourVision: TextView = view.findViewById(R.id.our_vision)

        val appApiService = AppApi.getInstanse()
        compositeDisposable.add(
            appApiService.getSettings().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(
                { response ->
                    if (response.status){
                        aboutusText.text = response.data.rows[0].languages[0].description
                        ourGoals.text = response.data.rows[0].ourGoals
                        ourVision.text = response.data.rows[0].ourVision
                        ourMessage.text = response.data.rows[0].ourMission
                    }
                },
                {
                    context?.makeToast("حدث خطا اثناء التحميل")
                    Log.d("AboutusFragment", "An error occurred", it)
                })
        )
        return view
    }
}