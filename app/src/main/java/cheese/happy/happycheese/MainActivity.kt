package cheese.happy.happycheese

import android.animation.Animator
import android.os.Bundle
import com.google.android.material.snackbar.Snackbar
import com.google.android.material.navigation.NavigationView
import androidx.core.view.GravityCompat
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.appcompat.app.AppCompatActivity
import android.view.Menu
import android.view.MenuItem
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.denzcoskun.imageslider.ImageSlider
import com.denzcoskun.imageslider.models.SlideModel
import jp.wasabeef.recyclerview.adapters.AlphaInAnimationAdapter
import jp.wasabeef.recyclerview.adapters.ScaleInAnimationAdapter
import jp.wasabeef.recyclerview.animators.FadeInAnimator
import jp.wasabeef.recyclerview.animators.FadeInUpAnimator
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.app_bar_main.*
import java.util.*
import com.google.android.material.resources.TextAppearance
import android.text.style.TextAppearanceSpan
import android.text.SpannableString
import android.util.Log
import android.view.View
import android.widget.ImageView
import androidx.core.view.ViewCompat.animate
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import com.synnapps.carouselview.CarouselView


class MainActivity : AppCompatActivity(), NavigationView.OnNavigationItemSelectedListener {

    lateinit var viewModel: MainViewModel
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
//        val res = resources
//        val dm = res.displayMetrics
//        val conf = res.configuration
//        conf.setLocale(Locale("ar")) // API 17+ only.
//        res.updateConfiguration(conf, dm)
        setContentView(R.layout.activity_main)
        viewModel = ViewModelProviders.of(this).get(MainViewModel::class.java)

        setSupportActionBar(toolbar)
//        title = resources.getString(R.string.app_name)
        supportActionBar?.title = ""
        if (this::viewModel.isInitialized) {
            var fragment: Fragment = MainFragment()
            when (viewModel.currentFragment) {
                ABOUTUS_FRAGMENT -> {
                    fragment = AboutUsFragment()
                }
                CONTACTUS_FRAGMENT -> {
                    fragment = ContactusFragment()
                }
                OFFERS_FRAGMENT -> {
                    fragment = OurOffersFragment()
                }
                GALLERY_FRAGMENT -> {
                    fragment = GalleryFragment()
                }
            }
            supportFragmentManager.beginTransaction().replace(R.id.fragment_container, fragment)
                .addToBackStack("")
                .commit()
        }
        val toggle = ActionBarDrawerToggle(
            this, drawer_layout, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close
        )
        drawer_layout.addDrawerListener(toggle)
        toggle.syncState()

        nav_view.setNavigationItemSelectedListener(this)

        changeMenuItemTextStyle(R.id.main_page)
        changeMenuItemTextStyle(R.id.our_offers)
        changeMenuItemTextStyle(R.id.gallery)
        changeMenuItemTextStyle(R.id.contact_us)
        changeMenuItemTextStyle(R.id.about_us)


    }

    private fun changeMenuItemTextStyle(id: Int) {

        val menuItem = nav_view.menu.findItem(id)
        val menuItemTitle = SpannableString(menuItem.title)
        when (id) {
            R.id.main_page -> {
                menuItemTitle.setSpan(TextAppearanceSpan(this, R.style.mainPageTextStyle), 0, menuItemTitle.length, 0)
            }
            R.id.about_us -> {
                menuItemTitle.setSpan(TextAppearanceSpan(this, R.style.aboutUsStyle), 0, menuItemTitle.length, 0)

            }
            R.id.our_offers -> {
                menuItemTitle.setSpan(TextAppearanceSpan(this, R.style.ourOffersStyle), 0, menuItemTitle.length, 0)
            }
            R.id.gallery -> {
                menuItemTitle.setSpan(TextAppearanceSpan(this, R.style.galleryStyle), 0, menuItemTitle.length, 0)

            }
            R.id.contact_us -> {
                menuItemTitle.setSpan(TextAppearanceSpan(this, R.style.contactUsStyle), 0, menuItemTitle.length, 0)

            }
        }
        menuItem.title = menuItemTitle

    }

    override fun onBackPressed() {
        if (drawer_layout.isDrawerOpen(GravityCompat.START)) {
            drawer_layout.closeDrawer(GravityCompat.START)
        } else {

            Log.d("tessss", "${supportFragmentManager.backStackEntryCount}")
            if (supportFragmentManager.backStackEntryCount == 1) {
                finish()
                return
            } else {
                super.onBackPressed()
            }
        }
    }

//    override fun onCreateOptionsMenu(menu: Menu): Boolean {
//        // Inflate the menu; this adds items to the action bar if it is present.
//        menuInflater.inflate(R.menu.main, menu)
//        return true
//    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        when (item.itemId) {
            R.id.action_settings -> return true
            else -> return super.onOptionsItemSelected(item)
        }
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        // Handle navigation view item clicks here.
        var fragment: Fragment? = null
        when (item.itemId) {
            R.id.main_page -> {
                fragment = MainFragment()
                viewModel.currentFragment = MAIN_FRAGMENT
            }
            R.id.about_us -> {
                fragment = AboutUsFragment()
                viewModel.currentFragment = ABOUTUS_FRAGMENT

            }

            R.id.our_offers -> {
                fragment = OurOffersFragment()
                viewModel.currentFragment = OFFERS_FRAGMENT

            }

            R.id.gallery -> {
                fragment = GalleryFragment()
                viewModel.currentFragment = GALLERY_FRAGMENT

            }

            R.id.contact_us -> {
                fragment = ContactusFragment()
                viewModel.currentFragment = CONTACTUS_FRAGMENT

            }
        }
        fragment?.let {
            supportFragmentManager.beginTransaction().replace(R.id.fragment_container, it)
                .addToBackStack("").commit()
        }


        drawer_layout.closeDrawer(GravityCompat.START)
        return true
    }
}
