package cheese.happy.happycheese

import android.animation.Animator
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import cheese.happy.happycheese.network.AppApi
import com.bumptech.glide.Glide
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers

class GalleryFragment : Fragment() {

    private val compositeDisposable = CompositeDisposable()
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.gallery_fragment, container, false)

        val appApiService = AppApi.getInstanse()

        compositeDisposable.add(
            appApiService.getGallery().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(
                {
                    if (it.status) {
                        val currentImage: ImageView = view.findViewById(R.id.current_image)
                        Glide.with(currentImage.context).load(AppApi.GALLERY_IMG_URL + it.data.galleries[0].image.name)
                            .into(currentImage)
                        val imageRecycler: RecyclerView = view.findViewById(R.id.images)
                        imageRecycler.adapter =
                            GalleryImageAdapter(object : GalleryImageAdapter.OnGalleryItemClickListener {
                                override fun onGalleryItemClicked(imgName: String) {
                                    Glide.with(this@GalleryFragment).load(AppApi.GALLERY_IMG_URL + imgName).into(currentImage)
                                }
                            }, it.data.galleries)
                        imageRecycler.layoutManager =
                            LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
                    }
                },
                {
                    context?.makeToast("An error occurred while loading gallery")
                })
        )
        return view
    }


    override fun onDestroyView() {
        super.onDestroyView()
        compositeDisposable.dispose()
    }
}