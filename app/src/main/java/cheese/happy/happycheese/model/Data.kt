package cheese.happy.happycheese.model


import com.google.gson.annotations.SerializedName

data class Data(
    @SerializedName("contact")
    val contact: Contact
)