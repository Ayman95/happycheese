package cheese.happy.happycheese

import android.animation.Animator
import android.app.ProgressDialog
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import cheese.happy.happycheese.network.AppApi
import com.bumptech.glide.Glide
import com.synnapps.carouselview.CarouselView
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers

class MainFragment : Fragment() {

    var currentPosition = -1

    val compositeDisposable = CompositeDisposable()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = layoutInflater.inflate(R.layout.main_fragment, container, false)

        val itemsRecycler: RecyclerView = view.findViewById(R.id.items_recycler)


        val dialog = ProgressDialog.show(context, "loading data", "").apply { setCancelable(false) }
        val appApiService = AppApi.getInstanse()
        compositeDisposable.add(
            appApiService.getSlider().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(
                {
                    if (it.status) {
                        var sampleImages = it.data.sliders
                        val carouselView: CarouselView = view.findViewById(R.id.carouselView)
                        dialog.dismiss()
                        carouselView.setViewListener { position ->
                            val view: View = layoutInflater.inflate(R.layout.carousel_item_layout, null)
                            val image: ImageView = view.findViewById(R.id.image)
                            val title: TextView = view.findViewById(R.id.title)
                            val desc: TextView = view.findViewById(R.id.desc)
                            Glide.with(image.context).load(AppApi.SLIDER_IMG_URL + sampleImages[position].image.name)
                                .into(image)

                            title.text = it.data.sliders[position].languages[0].title
                            desc.text = it.data.sliders[position].languages[0].description
                            return@setViewListener view
                        }
                        carouselView.pageCount = sampleImages.size

                    }
                },
                {
                    context?.makeToast("حدث خطا اثناء التحميل")
                    Log.e("mainFragment", "An error", it)
                    dialog.dismiss()
                })
        )


        compositeDisposable.add(
            appApiService.getFeatures().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(
                {
                    if (it.status) {
                        itemsRecycler.adapter =
                            ItemAdapter(context, it.data.features, object : ItemAdapter.OnItemClickListener {
                                override fun onItemClicked(position: Int, holder: ItemAdapter.ItemViewHolder) {
                                    if (currentPosition != -1 && currentPosition != position) {
                                        (itemsRecycler.getChildViewHolder(itemsRecycler.getChildAt(currentPosition)) as ItemAdapter.ItemViewHolder).apply {
                                            desc.apply {
                                                alpha = 1f
                                                animate().alpha(0f).setDuration(1000)
                                                    .setListener(object : Animator.AnimatorListener {
                                                        override fun onAnimationRepeat(animation: Animator?) {
                                                        }


                                                        override fun onAnimationEnd(animation: Animator?) {
                                                            this@apply.visibility = View.GONE
                                                        }

                                                        override fun onAnimationCancel(animation: Animator?) {
                                                        }


                                                        override fun onAnimationStart(animation: Animator?) {
                                                        }
                                                    })
                                            }
                                        }
                                    }
                                    currentPosition = position
                                    if (holder.desc.visibility == View.GONE) {
                                        holder.desc.apply {
                                            alpha = 0f
                                            visibility = View.VISIBLE
                                            animate().alpha(1f).setDuration(1000).setListener(null)
                                        }
                                    } else {
                                        holder.desc.apply {
                                            alpha = 1f
                                            animate().alpha(0f).setDuration(1000)
                                                .setListener(object : Animator.AnimatorListener {
                                                    override fun onAnimationRepeat(animation: Animator?) {
                                                    }


                                                    override fun onAnimationEnd(animation: Animator?) {
                                                        holder.desc.visibility = View.GONE
                                                    }

                                                    override fun onAnimationCancel(animation: Animator?) {
                                                    }


                                                    override fun onAnimationStart(animation: Animator?) {
                                                    }
                                                })
                                        }
                                    }
                                }
                            })
                        itemsRecycler.layoutManager = LinearLayoutManager(context)
                        itemsRecycler.isNestedScrollingEnabled = true
                    }
                },
                {

                })
        )


        val contactInfo: TextView = view.findViewById(R.id.contact_info)
        val address: TextView = view.findViewById(R.id.address)

        val facebook: ImageView = view.findViewById(R.id.facebook)
        val google: ImageView = view.findViewById(R.id.google)
        val insta: ImageView = view.findViewById(R.id.insta)
        val twitter: ImageView = view.findViewById(R.id.twitter)

        compositeDisposable.add(
            appApiService.getSettings().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(
                { response ->
                    if (response.status) {
                        contactInfo.text = "${response.data.rows[0].phone} \n ${response.data.rows[0].email}"
                        address.text = response.data.rows[0].address
                        val listener = View.OnClickListener {
                            var url = "www.google.com"
                            when (it.id) {
                                R.id.facebook -> {
                                    url = response.data.rows[0].facebook
                                }

                                R.id.insta -> {
                                    url = response.data.rows[0].instagram

                                }

                                R.id.google -> {
                                    url = response.data.rows[0].googlePlus

                                }

                                R.id.twitter -> {
                                    url = response.data.rows[0].twitter

                                }
                            }

                            val webpage: Uri = Uri.parse(url)
                            val intent = Intent(Intent.ACTION_VIEW, webpage)
                            if (intent.resolveActivity(context?.packageManager) != null) {
                                startActivity(intent)
                            }

                        }
                        facebook.setOnClickListener(listener)
                        google.setOnClickListener(listener)
                        insta.setOnClickListener(listener)
                        twitter.setOnClickListener(listener)
                    }
                },
                {
                    context?.makeToast("حدث خطا اثناء التحميل")
                    Log.e("MainFragment", "An Error ", it)
                })
        )
        return view
    }


    override fun onDestroy() {
        super.onDestroy()
        compositeDisposable.dispose()
    }
}