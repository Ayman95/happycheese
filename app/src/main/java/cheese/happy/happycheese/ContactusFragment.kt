package cheese.happy.happycheese

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ProgressBar
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import cheese.happy.happycheese.network.AppApi
import com.google.android.material.textfield.TextInputEditText
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers

class ContactusFragment : Fragment() {
    val compositeDisposable = CompositeDisposable()
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = layoutInflater.inflate(R.layout.contact_us_fragment, container, false)
        val name: TextInputEditText = view.findViewById(R.id.name)
        val email: TextInputEditText = view.findViewById(R.id.email)
        val phone: TextInputEditText = view.findViewById(R.id.phone)
        val title: TextInputEditText = view.findViewById(R.id.title)
        val message: TextInputEditText = view.findViewById(R.id.message)
        val sendBtn: Button = view.findViewById(R.id.send_btn)
        val loader: ProgressBar = view.findViewById(R.id.loader)

        sendBtn.setOnClickListener {
            sendBtn.visibility = View.GONE
            loader.visibility = View.VISIBLE
            val appApiService = AppApi.getInstanse()
            name.text?.toString()?.let { nameStr ->
                email.text?.toString()?.let { emailStr ->
                    phone.text?.toString()?.let { phoneStr ->
                        title.text?.toString()?.let { titleStr ->
                            message.text?.toString()?.let { messageStr ->
                                compositeDisposable.add(
                                    appApiService.contactus(
                                        nameStr,
                                        emailStr,
                                        phoneStr,
                                        titleStr,
                                        messageStr
                                    )
                                        .subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
                                        .subscribe({
                                            if (it.status) {
                                                sendBtn.visibility = View.VISIBLE
                                                loader.visibility = View.GONE
                                                context?.makeToast("Your message has been sent")
                                                (context as AppCompatActivity).supportFragmentManager.beginTransaction()
                                                    .replace(R.id.fragment_container, MainFragment()).addToBackStack("")
                                                    .commit()
                                            }
                                        }, {
                                            context?.makeToast("An error occurred")
                                            Log.e("contact us", "an error", it)
                                            sendBtn.visibility = View.VISIBLE
                                            loader.visibility = View.GONE
                                        })
                                )
                            }
                        }

                    }

                }

            }
        }
        return view
    }


    override fun onDestroyView() {
        super.onDestroyView()
        compositeDisposable.dispose()

    }
}