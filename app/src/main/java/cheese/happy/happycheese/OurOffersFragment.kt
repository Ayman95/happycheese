package cheese.happy.happycheese

import android.animation.Animator
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import cheese.happy.happycheese.network.AppApi
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers

class OurOffersFragment : Fragment() {
    var currentPosition = -1
    private val compositeDisposable = CompositeDisposable()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.our_offers_fragment, container, false)
        val appApiService = AppApi.getInstanse()

        compositeDisposable.add(
            appApiService.getFeatures().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(
                {
                    if (it.status) {
                        val itemsRecycler: RecyclerView = view.findViewById(R.id.items_recycler)
                        itemsRecycler.adapter = ItemAdapter(context, it.data.features,object : ItemAdapter.OnItemClickListener {
                            override fun onItemClicked(position: Int, holder: ItemAdapter.ItemViewHolder) {
                                if (currentPosition != -1 && currentPosition != position) {
                                    (itemsRecycler.getChildViewHolder(itemsRecycler.getChildAt(currentPosition)) as ItemAdapter.ItemViewHolder).apply {
                                        desc.apply {
                                            alpha = 1f
                                            animate().alpha(0f).setDuration(1000)
                                                .setListener(object : Animator.AnimatorListener {
                                                    override fun onAnimationRepeat(animation: Animator?) {
                                                    }


                                                    override fun onAnimationEnd(animation: Animator?) {
                                                        this@apply.visibility = View.GONE
                                                    }

                                                    override fun onAnimationCancel(animation: Animator?) {
                                                    }


                                                    override fun onAnimationStart(animation: Animator?) {
                                                    }
                                                })
                                        }
                                    }
                                }
                                currentPosition = position
                                if (holder.desc.visibility == View.GONE) {
                                    holder.desc.apply {
                                        alpha = 0f
                                        visibility = View.VISIBLE
                                        animate().alpha(1f).setDuration(1000).setListener(null)
                                    }
                                } else {
                                    holder.desc.apply {
                                        alpha = 1f
                                        animate().alpha(0f).setDuration(1000)
                                            .setListener(object : Animator.AnimatorListener {
                                                override fun onAnimationRepeat(animation: Animator?) {
                                                }


                                                override fun onAnimationEnd(animation: Animator?) {
                                                    holder.desc.visibility = View.GONE
                                                }

                                                override fun onAnimationCancel(animation: Animator?) {
                                                }


                                                override fun onAnimationStart(animation: Animator?) {
                                                }
                                            })
                                    }
                                }
                            }
                        })
                        itemsRecycler.layoutManager = LinearLayoutManager(context)
                        itemsRecycler.isNestedScrollingEnabled = true
                    }
                },
                {

                })
        )
        return view
    }


    override fun onDestroyView() {
        super.onDestroyView()
        compositeDisposable.dispose()
    }
}