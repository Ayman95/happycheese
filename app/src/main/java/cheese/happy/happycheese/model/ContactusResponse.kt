package cheese.happy.happycheese.model


import com.google.gson.annotations.SerializedName

data class ContactusResponse(
    @SerializedName("data")
    val `data`: Data,
    @SerializedName("errors")
    val errors: List<Any>,
    @SerializedName("msg")
    val msg: String,
    @SerializedName("status")
    val status: Boolean
)