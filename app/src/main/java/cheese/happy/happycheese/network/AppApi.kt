package cheese.happy.happycheese.network

import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

class AppApi {


    companion object {
        val API_URL = "http://saudihappycheese.com/api/"
        val GALLERY_IMG_URL = "http://saudihappycheese.com/img/galleries/"
        val SLIDER_IMG_URL = "http://saudihappycheese.com/img/sliders/"
        val FEATURE_IMG_URL = "http://saudihappycheese.com/img/features/"

        fun getInstanse(): AppApiService {
            val client = OkHttpClient.Builder().readTimeout(3, TimeUnit.MINUTES).writeTimeout(3, TimeUnit.MINUTES)
                .connectTimeout(3, TimeUnit.MINUTES).build()
            return Retrofit.Builder().client(client).addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create()).baseUrl(API_URL).build()
                .create(AppApiService::class.java)
        }
    }
}